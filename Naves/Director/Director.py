'''
Created on 04/07/2013

@author: Fernando
'''

import pygame, sys

class Director(object):
    
    SCREEN_SIZE = SCREEN_WIDTH, SCREEN_HEIGHT = 700, 800
    
    
    def __init__(self):
        self.scene = None
        self.quitFlag = False
        self.screen = pygame.display.set_mode(Director.SCREEN_SIZE)
        self.clock = pygame.time.Clock()
        
    def setScene(self, scene):
        self.scene = scene
        
    def loop(self):
        
        while not self.quit():
            self.clock.tick(120)
            self.screen.fill()
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit
                    
            self.scene.onEvent(pygame.key.get_pressed())
            
            self.scene.onUpdate()
            
            self.scene.onDraw(self.screen)
            
            pygame.display.flip()
    
    def quit(self):
        self.quitFlag = True