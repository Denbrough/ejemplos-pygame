'''
Created on 04/07/2013

@author: Fernando
'''

import pygame

class Scene(object):
    
    def __init__(self, director):
        self.director = director
        self.nave = Nave()
        
    def onDraw(self, screen):
        pass
    
    def onEvent(self, keys):
        pass
    
    def onUpdate(self):
        pass


class Nave(object):
    
    def __init__(self):
        self.pos = (None, None)
        self.tri = [[], [], []]
    
    def draw(self):
        pass