'''
Created on 03/07/2013

@author: Fernando
'''

import pygame, sys
from Main.Colores import Colores

class Director(object):
    
    SCREEN_SIZE = SCREEN_WIDTH, SCREEN_HEIGHT = 700, 800
    
    def __init__(self):
        self.__scene = None
        self.__mainScene = None
        self.quitFlag = False
        self.screen = pygame.display.set_mode(Director.SCREEN_SIZE)
        self.clock = pygame.time.Clock()
        self.timeDelta = 30.0
    
    def loop(self):
        pygame.key.set_repeat(1, 1)

        while not self.quitFlag:
            self.timeDelta += self.clock.tick(120)
            self.screen.fill(Colores.black)
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                    
                #if event.type == pygame.KEYDOWN:
                #    self.__scene.onEvent(event)

            # Capturamos el boton apreta y la posicion del raton
            mouse = self.__getMouse(self.timeDelta)
            
            # Pasamos las teclas pulsadas y la posicion del raton
            self.__scene.onEvent(pygame.key.get_pressed(), mouse)
            
            # Actualiza la escena
            self.__scene.onUpdate()
            
            # Redibuja la escena
            self.__scene.onDraw(self.screen)
            
            pygame.display.flip()
    
    def __getMouse(self, timeDelta):
        if self.timeDelta >= 100:
            self.timeDelta = 0
            return (pygame.mouse.get_pressed(), pygame.mouse.get_pos())
        else:
            return ((0,0,0), (0,0))
    
    def setScene(self, scene:"Scene"):
        self.__scene = scene
        self.loop()
        
    def quit(self):
        self.quitFlag = True
    
    def setMainScene(self, scene):
        self.__mainScene = scene
    
    def toMainScene(self):
        self.setScene(self.__mainScene)