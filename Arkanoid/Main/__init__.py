

import pygame, sys
from Main.Colores import Colores
from random import randint


SCREEN_SIZE = SCREEN_WIDTH, SCREEN_HEIGHT = 700, 800

def main():
    screen = pygame.display.set_mode(SCREEN_SIZE)
    pygame.display.set_caption("Me gusta el polen")
    pygame.key.set_repeat(1, 1)
    clock = pygame.time.Clock()
    
    pala = [50, SCREEN_HEIGHT - 25, 100, 16]
    radio = 8
    pelota = [pala[0] + pala[2]//2, pala[1] - radio, radio]
    speed = 2
    ball_speed = [1, -1]
    game = False
    finished = False
    
    while not finished:
        clock.tick(120)
        screen.fill((0, 0, 0))
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT and pala[0] + speed + pala[2] <= SCREEN_WIDTH :
                    pala[0] += speed
                elif event.key == pygame.K_LEFT and pala[0] - speed >= 0:
                    pala[0] -= speed
                elif event.key == pygame.K_SPACE:
                    game = True
        
        # Dibujamos la pala
        pygame.draw.rect(screen, Colores.red, pala)       
        
        # Dibujamos la pelota  
        pygame.draw.circle(screen, Colores.white, (pelota[0], pelota[1]), pelota[2])
        # Si el juego aun no a empezado la pelota debe seguir en el centro de la pala
        if game == False:
            pelota[0] = pala[0] + pala[2]//2
            pelota[1] = pala[1] - radio
        
        # Si el juego empieza las posiciones de la pelota y su direccion deben cambiar
        else:
            if pelota[0] + ball_speed[0] + radio < SCREEN_WIDTH:
                pelota[0] += ball_speed[0]
            elif pelota[0] + ball_speed[0] + radio > 0:
                ball_speed[0] = -1
            else:
                ball_speed[0] = 1
           
            
            if pelota[1] + ball_speed[1] + radio < SCREEN_HEIGHT:
                pelota[1] += ball_speed[1]
            else:
                ball_speed[1] = 1
        pygame.display.flip()

if __name__ == "__main__":
    pygame.init()
    main()