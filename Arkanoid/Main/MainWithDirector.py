'''
Created on 03/07/2013

@author: Fernando
'''

from Director import Director
from Scene import MainScene
import pygame


def main():
    director = Director()
    director.setMainScene(MainScene(director))
    director.toMainScene()

if __name__ == '__main__':
    pygame.init()
    main()