'''
Created on 03/07/2013

@author: Fernando
'''

from Director import Director
from Main.Colores import Colores
import pygame
from pygame import Color

class Scene(object):
    '''
    Representa una escena jugable
    '''
    def __init__(self, director:"Director"):
        self.director = director
        self.pala = Pala()
        self.pelota = Pelota()
        self.speed = 2
        self.game = False
    
    def onDraw(self, screen):
        self.pala.draw(screen)
        self.pelota.draw(screen)
        pygame.draw.circle(screen, Colores.green, (0,0), 5)
    
    def onUpdate(self):
        self.pelota.update((self.pala.rect[0] + self.pala.rect[2]//2,self.pala.rect[1]), self.game)
    
    def onEvent(self, keyButtons, mouseButtons):
        if keyButtons[pygame.K_RIGHT] and self.pala.rect[0] + self.speed + self.pala.rect[2] <= Director.SCREEN_WIDTH :
            self.pala.updatePos(self.speed)
        elif keyButtons[pygame.K_LEFT] and self.pala.rect[0] - self.speed >= 0:
            self.pala.updatePos(-self.speed)
        if keyButtons[pygame.K_SPACE]:
            self.game = True
            
        if keyButtons[pygame.K_ESCAPE]:
            self.director.toMainScene()

    
class Pala(object):
    
    WIDTH = 100
    HEIGHT = 16
    
    def __init__(self, xStart:"int" = Director.SCREEN_WIDTH//2 - 100//2):
        self.rect=[xStart, Director.SCREEN_HEIGHT - 25, Pala.WIDTH, Pala.HEIGHT]
        self.color = Colores.red
        
    def draw(self, screen):
        pygame.draw.rect(screen, self.color, self.rect)
        
    def updatePos(self, speed):
        self.rect[0] += speed


class Pelota(object):
    
    def __init__(self, rad:"int" = 8):
        self.circ = [None, None, rad]
        self.color = Colores.white
        self.ball_speed = [-2, -2]
        
    def draw(self, screen):
        pygame.draw.circle(screen, self.color, (self.circ[0], self.circ[1]), self.circ[2])
        
    def update(self, pos,  game):
        if game == False:
            self.circ[0] = pos[0]
            self.circ[1] = pos[1] - self.circ[2]
        
        # Si el juego empieza las posiciones de la pelota y su direccion deben cambiar
        else:
            if 0 < self.circ[0] + self.ball_speed[0] + self.circ[2] < Director.SCREEN_WIDTH:
                self.circ[0] += self.ball_speed[0]
            elif self.circ[0] + self.ball_speed[0] + self.circ[2] > 0:
                self.ball_speed[0] = -2
            else:
                self.ball_speed[0] = 2
           
           
            aux = self.circ[1] + self.ball_speed[1]
            if 0 < aux + self.circ[2] and aux < Director.SCREEN_HEIGHT:
                self.circ[1] += self.ball_speed[1]
            else:
                self.ball_speed[1] = 2
                
            if self.circ[1] + self.circ[2] >= pos[1]:
                self.ball_speed[1]=-1 


class EditorScene(object):
    
    def __init__(self, director):
        pass
    
    def onDraw(self, screen):
        pass
    
    def onEvent(self, mouseButtons):
        pass
    
    def onUpdate(self):
        pass
    
    
class MainScene(object):
    
    def __init__(self, director):
        self.director = director
        
        buttonSize = (300, 100)
        self.buttonNivel = [[Director.SCREEN_WIDTH//2 - buttonSize[0]//2, 200, buttonSize[0], buttonSize[1]], "Level"]
        self.buttonEditor = [[self.buttonNivel[0][0], 300 + buttonSize[1], buttonSize[0], buttonSize[1]], "Editor"]
        self.botonera = (self.buttonNivel, self.buttonEditor)
    
    
    def onDraw(self, screen):
        pygame.draw.rect(screen, Colores.green, self.buttonNivel[0])
        pygame.draw.rect(screen, Colores.red, self.buttonEditor[0])
    
    def onEvent(self, keyButtons, mouseButtons):
        if mouseButtons[0][0] == True:
            for boton in self.botonera:
                if self.__caeDentro(boton[0], mouseButtons[1]):
                    if boton[1] == "Level":
                        self.director.setScene(Scene(self.director))
    
    def onUpdate(self):
        pass
    
    def __caeDentro(self, boton, pos):
        dentroX = boton[0] < pos[0] < boton[0] + boton[2]
        dentroY = boton[1] < pos[1] < boton[1] + boton[3]
        return dentroX and dentroY
        